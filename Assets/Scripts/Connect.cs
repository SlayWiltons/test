﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connect : MonoBehaviour
{
    public static Vector3 StartPosition;
    public static Vector3 EndPosition;    
    public GameObject objLinePrefab;
    private GameObject hitClickObj;
    private GameObject objDragThrough;  //Объект, через который будет проходить линия от курсора
    private LineRenderer lineRend;
    private Vector3 screenPoint;
    private Vector3 offset;
    private bool OnDragging = false;

    void Start()
    {
        lineRend = GameObject.Find("DraggingLine").GetComponent<LineRenderer>();
    }

    void OnMouseDown()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            hitClickObj = hit.transform.gameObject;
            screenPoint = Camera.main.WorldToScreenPoint(hitClickObj.transform.position);
            offset = hitClickObj.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z)); //offset позволяет "отдалить" курсор до плоскости объекта
            if (hitClickObj.GetComponent<MeshRenderer>().material.color == Color.white)
            {
                hitClickObj.GetComponent<MeshRenderer>().material.color = Color.yellow;
                StartPosition = hitClickObj.transform.position;
                for (int i = 0; i < Spawner.objConnectors.Length; i++)
                {
                    if (Spawner.objConnectors[i].GetComponent<MeshRenderer>().material.color != Color.yellow)
                    {
                        Spawner.objConnectors[i].GetComponent<MeshRenderer>().material.color = Color.blue;
                    }
                }
            }
            else
            {
                if (hitClickObj.GetComponent<MeshRenderer>().material.color == Color.yellow)
                {
                    DefaultColor();
                }
            }
            if (hitClickObj.GetComponent<MeshRenderer>().material.color == Color.blue)
            {
                EndPosition = hitClickObj.transform.position;
                for (int i = 0; i < Spawner.objConnectors.Length; i++)
                {
                    if(Spawner.objConnectors[i].GetComponent<MeshRenderer>().material.color == Color.yellow)
                    {
                        StartPosition = Spawner.objConnectors[i].transform.position;
                    }
                    Spawner.objConnectors[i].GetComponent<MeshRenderer>().material.color = Color.white;
                }
                Instantiate(objLinePrefab); //Построение линии между "коннекторами"                
            }            
        } 
    }

    void OnMouseDrag()
    {
        if (hitClickObj.GetComponent<MeshRenderer>().material.color == Color.white)
        {
            OnDragging = false;
        }
        else
        {
            OnDragging = true;
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                for (int i = 0; i < Spawner.objConnectors.Length; i++)
                {
                    if (hit.transform.position != StartPosition && hit.transform.position == Spawner.objConnectors[i].transform.position)
                    {
                        objDragThrough = hit.transform.gameObject;
                        if (hit.transform.gameObject.GetComponent<MeshRenderer>().material.color == Color.blue)
                        {
                            hit.transform.gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
                        }
                        if (hit.transform.position != objDragThrough.transform.position)
                        {
                            objDragThrough.GetComponent<MeshRenderer>().material.color = Color.blue;
                        }
                    }
                }
            }
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            lineRend.SetPosition(0, StartPosition);
            lineRend.SetPosition(1, curPosition);
        }
    }

    void OnMouseUp()
    {
        if (OnDragging == true)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);            
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.GetComponent<MeshRenderer>().material.color == Color.blue || hit.transform.gameObject == objDragThrough)   //Строим линию либо по нажатию на второй "коннектор", либо при остановке на нем линии от курсора
                {
                    EndPosition = hit.transform.gameObject.transform.position;
                    Instantiate(objLinePrefab);
                    DefaultColor();
                }                
            }
            if (!Physics.Raycast(ray, out hit) || hit.transform.gameObject.name != "Connector") 
            {
                DefaultColor();
            }
            OnDragging = false;
        }
        lineRend.SetPosition(0, new Vector3(0, 0, 0));
        lineRend.SetPosition(1, new Vector3(0, 0, 0));
    }

    public static void DefaultColor()   //Окраска всех "коннекторов" в изначальный цвет
    {
        for (int i = 0; i < Spawner.objConnectors.Length; i++)
        {
            Spawner.objConnectors[i].GetComponent<MeshRenderer>().material.color = Color.white;
        }
    }
}