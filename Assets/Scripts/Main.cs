﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{
    public float Radius = 10;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))    //Клик на пустом пространстве окрашивает все "коннекторы" в первоначальный цвет
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out hit))
            {
                Connect.DefaultColor();
            }
        }
        if (Input.GetMouseButton(0))    //Если линия, идущая за курсором, уходит с "коннектора"
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out hit) || Physics.Raycast(ray, out hit) && hit.transform.gameObject.name != "Connector")
            {
                for (int i = 0; i < Spawner.objConnectors.Length; i++)
                {
                    if (Spawner.objConnectors[i].GetComponent<MeshRenderer>().material.color != Color.white)
                    {
                        if (Spawner.objConnectors[i].transform.position != Connect.StartPosition)
                        {
                            Spawner.objConnectors[i].GetComponent<MeshRenderer>().material.color = Color.blue;
                        }
                    }
                }
            }
        }
    }
}