﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDrag : MonoBehaviour
{
    private Vector3 screenPoint;
    private Vector3 offset;
    private GameObject dragHitObject;

    void OnMouseDown()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            dragHitObject = hit.transform.parent.gameObject;
            screenPoint = Camera.main.WorldToScreenPoint(dragHitObject.transform.position);
            offset = dragHitObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
            for (int i = 0; i < Spawner.objConnectors.Length; i++)
            {
                Spawner.objConnectors[i].GetComponent<MeshRenderer>().material.color = Color.white;
            }
        }
    }

    void OnMouseDrag()  //Перемещение объекта при удержании курсора на платформе
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        dragHitObject.transform.position = curPosition;
    }
}