﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateLine : MonoBehaviour 
{
    private Vector3 StartP;
    private Vector3 EndP;
    private GameObject StartPoint;
    private GameObject EndPoint;

    void Start()    //Создание линии между "коннекторами"
    {
        StartP = Connect.StartPosition;
        EndP = Connect.EndPosition;
        for (byte i = 0; i < Spawner.objConnectors.Length; i++)
        {
            if (Spawner.objConnectors[i].transform.position == StartP)
            {
                StartPoint = Spawner.objConnectors[i];
            }
            if (Spawner.objConnectors[i].transform.position == EndP)
            {
                EndPoint = Spawner.objConnectors[i];
            }
        }
        gameObject.GetComponent<LineRenderer>().SetPosition(0, StartP);
        gameObject.GetComponent<LineRenderer>().SetPosition(1, EndP);
    }

    void Update()   //При перемещении "коннекторов", соединяющая линия меняет свое положение
    {
        gameObject.GetComponent<LineRenderer>().SetPosition(0, StartPoint.transform.position);
        gameObject.GetComponent<LineRenderer>().SetPosition(1, EndPoint.transform.position);
    }
}