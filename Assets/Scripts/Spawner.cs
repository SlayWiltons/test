﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject objParentPrefab;
    private Vector3 posStart;
    public static GameObject[] objPrefabClone;
    public static GameObject[] objConnectors;
    private Vector3 posCurrent;
    private float posShift;
    public byte prefCount = 10;

    void Start()
    {
        objPrefabClone = new GameObject [prefCount];
        objConnectors = new GameObject[prefCount];
        posShift = GameObject.Find("Main").GetComponent<Main>().Radius; //Радиус, ограничивающий область создания объектов       
        for (byte i = 0; i < prefCount; i++)
        {
            posCurrent = new Vector3 (Random.Range(posStart.x - posShift, posStart.x + posShift), 
                Random.Range(posStart.y - posShift, posStart.y + posShift),
                Random.Range(posStart.z - posShift, posStart.z + posShift));    //Случайные координаты создания объекта в пределах радиуса
            objPrefabClone[i] = Instantiate (objParentPrefab, posCurrent, Quaternion.identity);
            objConnectors[i] = objPrefabClone[i].transform.Find("Connector").gameObject;
        }
    }
}